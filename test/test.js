const request = require('supertest');
const app = require('../src/server');


describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Hello World/, done);
  });
  it('hast the correct version', function(done) {
    request(app)
      .get('/version')
      .expect(/0.0.0/, done);
  });
});

