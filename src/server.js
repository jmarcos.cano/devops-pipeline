const express = require('express');
var path = require('path');

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();

// TODO: probably retrieve it from package.json
const version = process.env.VERSION || "0.0.0";

app.get('/', function (req, res) {
    res.send('Hello World');
});

app.get('/version', function (req, res) {
    res.send({ "Version": `${version}` });
});

var detectMocha = require('detect-mocha');

if (detectMocha()) {
    // doSomethingFancy
    module.exports = app;
} else {
    app.listen(PORT);
    console.log('Running on http://localhost:' + PORT);
}

