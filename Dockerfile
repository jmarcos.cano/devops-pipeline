FROM --platform=linux/amd64 node:16-slim

WORKDIR /app


ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

# best practice
COPY package.json .

RUN  npm install

# copy source-code
COPY . .


ENTRYPOINT ["/tini", "--"]
# kubernetes command
CMD [ "node","src/server.js" ]

EXPOSE 3000

